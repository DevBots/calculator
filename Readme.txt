Calculator is designed to work with custom set of operations and order the calculations based on the operations priority.
So, task reduced to creating a data structure for operations and option to update it by custom lambda functions and simply build and iterate through AST.

architecture of the program:

1) create ICalculate interface which grab 2 numbers and operation
 *instead of that using import java.util.function.DoubleBinaryOperator;
2) create Operations list (dictionary) and add lambas to it. each lamba - one operation. add priorities to it if possible
calculator will be very simply extendable by user - just add your operation and it's priority
3) create Parser which transform a string divided by spaces to the list of numbers and operations
4) create Sorter which will sort the list of numbers and operations according it's priority. the result will be AST
5) create Executor which will get the structure of numbers and operations and execute operations.
6) add unit tests