import java.util.function.DoubleBinaryOperator;

/**
 * Created by User on 2/13/2017.
 */

@FunctionalInterface
public interface ICalculate {
    public DoubleBinaryOperator calculate();
}

