/**
 * Created by User on 2/13/2017.
 */
public class Operation {
    protected Integer priority;
    protected Character symbol;
    ICalculate execute;
    public Operation(Integer priority, Character symbol, ICalculate execute) {
        this.priority = priority;
        this.symbol = symbol;
        this.execute = execute;
    }
}