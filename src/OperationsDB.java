import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vsarnatckii on 2/13/2017.
 */
public class OperationsDB {
    // Map corresponds priority to a list of operations
    Map<Integer, Collection<Operation>> commands = new HashMap<>();

    public void addOperation(Integer priority, Character operationSymbol, ICalculate calc){

        // for example
        // DoubleBinaryOperator plus = (num1, num2) -> num1 + num2;
        // addOperations(2, "+", plus);

        Operation calculator = new Operation(priority, operationSymbol, calc);
        if (!commands.containsKey(priority))
                commands.put(priority, new ArrayList<>());
        commands.get(priority).add(calculator);
        }

}
